import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import thunk from 'redux-thunk'
import { createLogger } from 'redux-logger'

import { rootReducer } from '../reducers/'

import { api, paypal, business, login } from '../middleware';

export const workchewOrderStateKey = 'orders.workchew.preloadedState';

const enhancer = compose(
  applyMiddleware(thunk, api, paypal, business, login, createLogger()),
)

const configureStore = preloadedState => {
  const store = createStore(
    rootReducer,
    preloadedState,
    enhancer

  )

  return store
}

var preloadedState = {
  user: {
    name: "seth",
    email: "isethguy@gmail.com"
  },
  business: {
    cart: {
      showDialog: true,
      items: [],
      total: 0,
      subtotal: 0,
      taxes: 0,
      discounts: 0
    },
    menus: []
  }
}

var StoredState = localStorage.getItem(workchewOrderStateKey) || JSON.stringify(preloadedState);

preloadedState = JSON.parse(StoredState)
console.log(' preloadedState ', preloadedState)

export const store = configureStore(preloadedState);

store.subscribe(() => {

  localStorage.setItem(workchewOrderStateKey, JSON.stringify(store.getState()))
})