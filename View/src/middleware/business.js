
import { getMenuItems } from '../Components/Business/actions/itemsApi'
// A Redux middleware that interprets actions with CALL_API info specified.
// Performs the call and promises when such actions are dispatched.

const API_ROOT = window.location.port == 3000 ? "http://localhost:8080" : ""


const callApi = (endpoint, config = {}) => {

  const fullUrl = `${API_ROOT}${endpoint}`

  var responseType = "json"

  if (config && config.responseType) {

    responseType = config.responseType;

  }

  return fetch(fullUrl, config)

    .then(response => response[responseType]().then(json => {

      if (!response.ok) {
        return Promise.reject(json)
      }

      return Object.assign({}, json)
    }))
}


const menuItemPath = "/menu-items"

//
export default store => next => action => {

  if (action.type == 'GET_BUSINESS_MENUS_SUCCESS') {

    var state = store.getState();

    var endPoint = ""

    var menus = [...Object.keys(action.response).map(key => action.response[key])]

    menus.forEach((menu) => {

      store.dispatch(getMenuItems({
        mid: menu._id
      }))

    })

  } else if (action.type == 'PUT_BUSINESS_MENU_SUCCESS' || action.type == 'DELETE_BUSINESS_MENU_SUCCESS') {

    window.location.reload()

    return next(action)

  } else if (action.type == 'DELETE_MENU_ITEM_SUCCESS') {
    var hash = window.location.hash;

    var split = hash.split("/")
    store.dispatch(getMenuItems({
      mid: split[2]
    }))



  } else {

    return next(action)
  }

  return next(action)


}