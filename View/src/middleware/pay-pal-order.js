
import { postOrderActionCreator } from '../Components/Business/Receipt/actions'
// A Redux middleware that interprets actions with CALL_API info specified.
// Performs the call and promises when such actions are dispatched.
export default store => next => action => {

  console.log("@ api middle", action)

  if (action.type == 'POST_ORDER_SUCCESS') {

    var state = store.getState();

    var {cart} = state;

    console.log(cart, "POST_ORDER_SUCCESS @ middle ware", action)

    window.location.hash = `receipt?order=${action.response.postOrdersResponse.orderID}&payment=${action.response.postOrdersResponse.paymentID}`


  } else {

    return next(action)
  }



}