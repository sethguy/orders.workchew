import React from 'react'
import { Route } from 'react-router'
import App from './Components/App'
import { HashRouter } from 'react-router-dom'

export default <Route exact path="/" component={ App }></Route>