import { CALL_API, Schemas } from '../../../../middleware/api'

export const GET_ORDERS = 'GET_ORDERS'
export const GET_ORDERS_SUCCESS = 'GET_ORDERS_SUCCESS'
export const GET_ORDERS_FAILURE = 'GET_ORDERS_FAILURE'

const orderPath = "/orders"

const getOrdersActionCreator = (query) => {

  var queryString = "";

  if (Object.keys(query).length) {

    queryString = Object.keys(query).reduce((queryString, key, i) => {

      if (i == 0) {
        return `${key}=${query[key]}`

      } else {
        return `&${key}=${query[key]}`
      }

    }, '')

  }

  return {

    [CALL_API]: {
      types: [GET_ORDERS, GET_ORDERS_SUCCESS, GET_ORDERS_FAILURE],
      endpoint: `${orderPath}?${queryString}`,
      config: {

        method: "GET",

        headers: {
          "Content-type": "application/json"
        },
      }
    }
  }

}

export const getOrders = (data) => (dispatch, getState) => {

  return dispatch(getOrdersActionCreator(data))
}

export const POST_ORDER = 'POST_ORDER'
export const POST_ORDER_SUCCESS = 'POST_ORDER_SUCCESS'
export const POST_ORDER_FAILURE = 'POST_ORDER_FAILURE'

export const postOrderActionCreator = (body) => ({
  [CALL_API]: {
    types: [POST_ORDER, POST_ORDER_SUCCESS, POST_ORDER_FAILURE],
    endpoint: `${orderPath}`,
    config: {

      method: "POST",

      headers: {
        "Content-type": "application/json"
      },

      body: JSON.stringify(body)

    }
  }
})

export const postOrder = (data) => (dispatch, getState) => {

  return dispatch(postOrderActionCreator(data))
}

export const PUT_ORDER = 'PUT_ORDER'
export const PUT_ORDER_SUCCESS = 'PUT_ORDER_SUCCESS'
export const PUT_ORDER_FAILURE = 'PUT_ORDER_FAILURE'

const putOrderActionCreator = (body) => ({
  [CALL_API]: {
    types: [PUT_ORDER, PUT_ORDER_SUCCESS, PUT_ORDER_FAILURE],
    endpoint: `${orderPath}`,
    config: {

      method: "PUT",

      headers: {
        "Content-type": "application/json"
      },

      body: JSON.stringify(body)

    }
  }
})

export const putOrder = (data) => (dispatch, getState) => {

  return dispatch(putOrderActionCreator(data))
}

export const DELETE_ORDER = 'DELETE_ORDER'
export const DELETE_ORDER_SUCCESS = 'DELETE_ORDER_SUCCESS'
export const DELETE_ORDER_FAILURE = 'DELETE_ORDER_FAILURE'

const deleteOrderActionCreator = (body) => ({
  [CALL_API]: {
    types: [DELETE_ORDER, DELETE_ORDER_SUCCESS, DELETE_ORDER_FAILURE],
    endpoint: `${orderPath}`,
    config: {

      method: "PUT",

      headers: {
        "Content-type": "application/json"
      },

      body: JSON.stringify(body)

    }
  }
})

export const deleteOrder = (data) => (dispatch, getState) => {

  return dispatch(deleteOrderActionCreator(data))
}