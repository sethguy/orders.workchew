import React, { Component } from 'react';
import { connect } from 'react-redux'

import RaisedButton from 'material-ui/RaisedButton';
import { Card, CardActions, CardHeader, CardMedia, CardTitle, CardText } from 'material-ui/Card';

import { v4 } from 'uuid'

import { getOrders, postOrder } from './actions';

import { Title } from '../../shared/'

import CurrencyFormat from 'react-currency-format';

import TextField from 'material-ui/TextField';

export const ReceiptItems = (props) => {

  var {receipt} = props.business

  var total = 0

  if (receipt && receipt.transactions) {

    var {transactions} = receipt;

    var [transaction] = transactions;

    var {item_list, amount} = transaction

    var {items} = item_list

    total = amount
  }

  return (<div className="cart-items">
            { items.map((item, index) => (
                <Card key={ v4() }>
                  <CardTitle title={ <div onClick={ (event) => {
                                                      props.removeItemFromCart({
                                                        item,
                                                        index
                                                      })
                                                    } } className="w-100 d-flex justify-content-between">
                                       <span className="Brandon_bld">{ item.name }</span>
                                     </div> } subtitle={ <span className="Brandon_bld"><CurrencyFormat value={ item.price } displayType={ 'text' } thousandSeparator={ true } prefix={ '$' } /></span> } />
                </Card>
              )) }
          </div>)
}


class Checkout extends Component {

  constructor(props) {

    super(props);

    var {hash} = window.location;

    var [url, paramsString] = hash.split('?')

    var paramSplit = paramsString.split('&')

    var params = paramSplit.reduce((params, paramSplitString) => {

      var [key, value] = paramSplitString.split('=')

      return {
        ...params,
        [key]: value
      }

    }, {})

    console.log({
      paramsString,
      paramSplit,
      url,
      params
    })

    this.props.getOrders(
      params
    )

    this.state = {

      ...this.props,

    }

  }

  componentDidMount() {}

  render() {

    var {receipt} = this.props.business

    var total = 0

    if (receipt && receipt.transactions) {

      var transaction = receipt.transactions[0];

      var items = transaction.item_list.items;

      total = transaction.amount.total

    }

    return (

      <div className="check-out h-100 d-flex flex-column">
        <Title {...{ title: "Receipt Thank You" }} />
        <div className="check-out-body flex-1 d-flex flex-column">
          { this.props.business.receipt && <ReceiptItems {...this.props}/> }
          <br/>
          <div className="cart-totals">
            <div>
              <span className="Brandon_bld">Total:</span>
              <span className="Brandon_bld">{ <CurrencyFormat value={ total } displayType={ 'text' } thousandSeparator={ true } prefix={ '$' } /> }</span>
            </div>
          </div>
          <br/>
          <br/>
          <div className="send-receipt d-flex flex-column align-items-center">
            <h3 className="Brandon_bld">Send Receipt:</h3>
            <TextField onChange={ (event) => {
                                    this.setState({
                                      emailForReceipt: event.target.value
                                    })
                                  
                                  } } hintText="Email For Receipt" value={ this.state.emailForReceipt } floatingLabelText="Email For Receipt" />
            <br/>
            <RaisedButton className="p-1 Brandon_bld">
              <span className="Brandon_bld">Send Email Receipt</span>
            </RaisedButton>
          </div>
        </div>
      </div>

    )
  }

}

const mapStateToProps = (state, ownProps) => ({
  user: state.user,
  business: state.business
})

const CheckoutComponent = connect(mapStateToProps, {
  getOrders,
  postOrder
})(Checkout)

export default CheckoutComponent