import React, { Component } from 'react';
import { connect } from 'react-redux'

import RaisedButton from 'material-ui/RaisedButton';
import { Card, CardActions, CardHeader, CardMedia, CardTitle, CardText } from 'material-ui/Card';

import { v4 } from 'uuid'

import * as BusinessActions from '../actions';

import { Title } from '../../shared/'

import { CartItems } from '../Cart/'

import { placeButton } from './actions'

import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';

import CurrencyFormat from 'react-currency-format';

class Checkout extends Component {

  constructor(props) {

    super(props);

    this.state = {
      ...this.props,
    }
  }

  componentDidMount() {

    window.paypalCheckoutReady = () => {
      this.props.placeButton({
        elementKey: 'pay-button',
        cart: this.props.business.cart,
        business: this.props.business
      })

    }

  }

  render() {
    return (

      <div className="check-out h-100 d-flex flex-column">
        <Title {...{ title: "Check Out" }} />
        <div className="check-out-body flex-1 d-flex">
          <div className="order-summary-payments flex-1 d-flex align-items-center flex-column">
            <div style={ { borderBottom: "1px solid black" } }>
              <h1 className="my-1 Brandon_bld">Hi, review your order details:</h1>
            </div>
            <br />
            <div className="button row">
              <div id="pay-button">
              </div>
            </div>
          </div>
          <div className="order-summary flex-1 d-flex flex-column">
            <br/>
            <div className="my-1 Brandon_bld">
              <span className="Brandon_bld">ADD A TIP</span>
              <br/>
              <RaisedButton>
                10%
              </RaisedButton>
              <RaisedButton>
                15%
              </RaisedButton>
              <RaisedButton>
                18%
              </RaisedButton>
              <RaisedButton>
                20%
              </RaisedButton>
              <TextField className="mx-2" defaultValue={ this.props.business.cart.tip } floatingLabelText="tip amount" />
            </div>
            <div className="flex-1 scroll-y">
              <CartItems {...this.props.business}/>
            </div>
            <br/>
            <div className="cart-totals">
              <div className="px-2 d-flex justify-content-between align-items-center">
                <span className="Brandon_bld">SUBTOTAL</span>
                <span className="Brandon_bld"><CurrencyFormat value={ this.props.business.cart.subtotal } displayType={ 'text' } thousandSeparator={ true } prefix={ '$' } /></span>
              </div>
              <div className="px-2 d-flex justify-content-between align-items-center">
                <span className="Brandon_bld">TAXES</span>
                <span className="Brandon_bld"><CurrencyFormat value={ this.props.business.cart.taxes } displayType={ 'text' } thousandSeparator={ true } prefix={ '$' } /></span>
              </div>
              <div style={ { color: 'black', fontWeight: "bold" } } className="px-2 d-flex justify-content-between align-items-center">
                <h3 className="Brandon_bld">Total</h3>
                <h3 className="Brandon_bld"><CurrencyFormat value= { this.props.business.cart.total } displayType={ 'text' } thousandSeparator={ true } prefix={ '$' } /></h3>
              </div>
              <div className="px-2 d-flex justify-content-between align-items-center">
                <span className="Brandon_bld">TIP</span>
                <span className="Brandon_bld"><CurrencyFormat value= { this.props.business.cart.tip } displayType={ 'text' } thousandSeparator={ true } prefix={ '$' } /></span>
              </div>
              <div style={ { color: 'black', fontWeight: "bold" } } className="px-2 d-flex justify-content-between align-items-center">
                <h3 className="Brandon_bld">Total with Tip</h3>
                <h3 className="Brandon_bld"><CurrencyFormat value= { this.props.business.cart.total } displayType={ 'text' } thousandSeparator={ true } prefix={ '$' } /></h3>
              </div>
            </div>
            <br/>
          </div>
        </div>
      </div>

    )
  }

}

const mapStateToProps = (state, ownProps) => ({
  user: state.user,
  business: state.business
})

const CheckoutComponent = connect(mapStateToProps, {
  placeButton
})(Checkout)

export default CheckoutComponent