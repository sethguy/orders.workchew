/*
import restService from '../Services/restService.js'
import urlService from '../Services/urlService.js'
import userService from '../Services/userService.js'

import { loaderStream } from '../Components/shared/workLoader'

import { signUpDialogSubject } from '../Components/SignUp/UserSignUp'


var verifyPayPalTransaction = function(paypalTransactionData) {
  loaderStream.next(true)

  restService
    .post(urlService.payPal, {
      paypalTransactionData,

    }, {
      "x-api-access-token": paypalTransactionData.token
    })

    .subscribe((verifyPayPalTransactionResponse) => {

      loaderStream.next(false)

      var signUpMessage = "Sign up error try again"


      var {getPaymentStream} = verifyPayPalTransactionResponse

      if (getPaymentStream && getPaymentStream._id) {

        var {memberShipInfo} = getPaymentStream;

        if (memberShipInfo) {

          var {paymentAuth} = memberShipInfo

          if (paymentAuth && paymentAuth.lastPaymentId) {

            signUpMessage = " Sign up success ! , check your email for next steps"

          }

        }

      }

      signUpDialogSubject.next({
        dialogMsg: signUpMessage,
        showDialog: true,
        signUpComplete: true
      })

    })

}
*/


import { postOrderActionCreator } from '../Receipt/actions'


var placeButton = function(config, dispatch) {

  var {elementKey, cart, bussiness} = config;

  var {items, total} = cart;

  console.log(cart, 'at pay button')

  var paypal = window['paypal'];

  paypal.Button.render({

    // Set your environment

    env: 'sandbox', // sandbox | production

    // Specify the style of the button

    style: {
      label: 'buynow',
      // fundingicons: true, // optional
      branding: true, // optional
      fundingicons: true,
      size: 'medium', // small | medium | large | responsive
      shape: 'rect', // pill | rect
      color: 'blue' // gold | blue | silve | black
    },

    // PayPal Client IDs - replace with your own
    // Create a PayPal app: https://developer.paypal.com/developer/applications/create

    client: {
      sandbox: 'ARlX_FQiPMWYBBxHdmAuPnIh4lF0YChw3Ju8R6oPiFfPS7NW74VcNadMQmDuXUmOwlKO215-DIZ4rtVp',
    //production: '<insert production client id>'
    },

    // Wait for the PayPal button to be clicked

    payment: function(data, actions) {

      var token = "token"

      return actions.payment.create({
        transactions: [{
          amount: {
            total,
            currency: 'USD'
          },
          custom: token,
          item_list: {
            items: items.map(item => {

              var {_id, name, price} = item

              console.log("itemitemitemitemitem", item)

              return {
                ...{
                  url: `https://orders.workchew.com/item/${item._id}`,
                  name,
                  price: parseFloat(price)
                },
                quantity: 1,
                currency: "USD"
              }
            })
          }
        }]
      });

    },

    // Wait for the payment to be authorized by the customer

    onAuthorize: function(data, actions) {

      dispatch({
        type: "PAY_PAL_AUTHORIZE",
        data
      })

      return actions.payment.execute().then(function() {

        dispatch(postOrderActionCreator({
          ...data,
          bid: 1 //bussiness._id
        }))

      });
    }

  }, `#${elementKey}`);

}

export { placeButton }