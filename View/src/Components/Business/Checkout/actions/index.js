
import * as paypal from '../paypal'
const PLACE_BUTTTON = "PLACE_BUTTTON"

export const PayPalTypes = {

  PLACE_BUTTTON
}

const placeButtonCreator = (data) => {

  return {
    type: PLACE_BUTTTON,
    payload: {
      ...data,
    }
  }

}

export const placeButton = (data) => (dispatch, getState) => {

  paypal.placeButton(data, dispatch)

  return dispatch(placeButtonCreator(data))

}