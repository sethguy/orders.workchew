import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';

import MenuIcon from 'material-ui/svg-icons/navigation/menu';
import ActionCart from 'material-ui/svg-icons/action/add-shopping-cart';

import Menu, { MenuItem } from 'material-ui/Menu';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

import { Card, CardActions, CardHeader, CardMedia, CardTitle, CardText } from 'material-ui/Card';

import { v4 } from 'uuid'
import DeleteForever from 'material-ui/svg-icons/action/delete-forever';
import * as BusinessActions from '../actions';

import { connect } from 'react-redux'
import CurrencyFormat from 'react-currency-format';

export const CartItems = (props) => (
  <div className="scroll-y">
    { props.cart.items.map((item, index) => (
        <Card key={ v4() }>
          <CardTitle title={ <div onClick={ (event) => {
                                              props.removeItemFromCart({
                                                item,
                                                index
                                              })
                                            } } className="w-100 d-flex justify-content-between">
                               <span className="Brandon_bld">{ item.name }</span>
                               <span className="Brandon_bld"><CurrencyFormat value={ item.price } displayType={ 'text' } thousandSeparator={ true } prefix={ '$' } /></span>
                             </div> } />
        </Card>
      )) }
  </div>)

class CartDialog extends Component {

  constructor(props) {

    super(props);

    this.state = {

      ...this.props,
    }
  }

  render() {
    var {handleClose, state: {open}, props: {removeItemFromCart}} = this

    return (
      <Dialog title={ <div>
                  <span className="Brandon_bld">{ this.props.business.cart.items.length } items</span> in your order
                </div> } modal={ false } open={ this.props.open } onRequestClose={ this.props.handleClose }>
        <hr />
        <div style={ { maxHeight: 300 } } className="scroll-y">
          <CartItems style={ { maxHeight: 500 } } {...{ ...this.props.business, removeItemFromCart }}/>
        </div>
        <br/>
        <div className="cart-totals">
          <div className="px-2 d-flex justify-content-between align-items-center">
            <span className="Brandon_bld">SUBTOTAL</span>
            <span className="Brandon_bld"><CurrencyFormat value={ this.props.business.cart.subtotal } displayType={ 'text' } thousandSeparator={ true } prefix={ '$' } /></span>
          </div>
          <div className="px-2 d-flex justify-content-between align-items-center">
            <span className="Brandon_bld">TAXES</span>
            <span className="Brandon_bld"><CurrencyFormat value={ this.props.business.cart.taxes } displayType={ 'text' } thousandSeparator={ true } prefix={ '$' } /></span>
          </div>
          <div style={ { color: 'black', fontWeight: "bold" } } className="px-2 d-flex justify-content-between align-items-center">
            <h3 className="Brandon_bld">Total</h3>
            <h3 className="Brandon_bld"><CurrencyFormat value={ this.props.business.cart.total } displayType={ 'text' } thousandSeparator={ true } prefix={ '$' } /></h3>
          </div>
        </div>
        <br/>
        <div className="cart-actions">
          <RaisedButton primary={ true } className="w-100 d-flex" onClick={ (event) => {
                                                                              window.location.hash = `checkout`
                                                                            } }>
            <div className="h-100 px-2 d-flex justify-content-between align-items-center">
              <span className="Brandon_bld" style={ { color: 'white' } }>CHECKOUT{ '  (' }{ this.props.business.cart.items.length + ' ITEMS)' }</span>
              <span className="Brandon_bld" style={ { color: 'white' } }><CurrencyFormat value={ this.props.business.cart.total } displayType={ 'text' } thousandSeparator={ true } prefix={ '$' } /></span>
            </div>
          </RaisedButton>
        </div>
      </Dialog>
    )
  }

}

var {removeItemFromCart} = BusinessActions


const mapStateToProps = (state, ownProps) => ({
  user: state.user,
  business: state.business
})


const CartDialogComponent = connect(mapStateToProps, {
  removeItemFromCart
})(CartDialog)

export default CartDialogComponent