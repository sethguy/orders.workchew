import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux'

import { FlexTable, Title, MatFlexTable } from '../../shared/'

import * as businessActions from '../actions'

import * as ItemActions from '../actions/itemsApi'


import RaisedButton from 'material-ui/RaisedButton';
import { Card, CardActions, CardHeader, CardMedia, CardTitle, CardText } from 'material-ui/Card';

import { v4 } from 'uuid'
import TextField from 'material-ui/TextField';

import Dialog from 'material-ui/Dialog';
var {selectItemForCart, showAddItemModal} = businessActions

const onItemUpdate = (event, props, key, item, i) => {

  var {target: {value}} = event;

  var {updateStateItem} = props;

  updateStateItem({
    item,
    index: i,
    key,
    value
  })

}

const removeItem = (item, props) => {

  var {deleteMenuItem} = props;

  deleteMenuItem({
    _id: item._id
  })

}



const RemoveItemButton = (props) => {

  var businessInfo = JSON.parse(localStorage.getItem("orders.workchew.admin.business"))

  if (businessInfo._id) {

    return (
      <RaisedButton className="m-1" onClick={ (event) => {
                                        
                                          console.log(props)
                                        
                                          removeItem({
                                        
                                            _id: props.itemCheck._id
                                          }, props)
                                        
                                          props.hideRemoveItemCheck()
                                        
                                        
                                        } }>
        Remove Item
      </RaisedButton>
    )

  } else {

    return (<span></span>)

  }

}
const CloseRemoveItemButton = (props) => (
  <RaisedButton className="m-1" onClick={ (event) => {
                                        
                                          props.hideRemoveItemCheck()
                                        
                                        } }>
    Close
  </RaisedButton>
)





const onItemSelect = (item, props) => {

  var {putMenuItem} = props;

  putMenuItem(item)

}

const Items = (props) => props.items.map((item, i) => (
  <Card className="m-1" key={ v4() }>
    <CardTitle title={ item.name } subtitle={ item.price } />
    <CardText>
      <TextField onBlur={ (event) => onItemUpdate(event, props, 'name', item, i) } className="w-100" defaultValue={ item.name } floatingLabelText="Name" />
      <br />
      <TextField onBlur={ (event) => onItemUpdate(event, props, 'price', item, i) } className="w-100" defaultValue={ item.price } floatingLabelText="Price" />
      <br />
      <TextField onBlur={ (event) => onItemUpdate(event, props, 'description', item, i) } className="w-100" defaultValue={ item.description } floatingLabelText="Description" />
      <br />
    </CardText>
    <CardActions>
      <RaisedButton onClick={ (event) => onItemSelect(item, props) }>
        Save Update
      </RaisedButton>
      <RaisedButton onClick={ (event) => {
                              
                              
                                props.showRemoveItemCheck(item)
                              
                                //  removeItem(item, props)
                              
                              } }>
        remove Item
      </RaisedButton>
    </CardActions>
  </Card>
))

const Sections = (props) => props.sections.map((section) => (

  <Card key={ v4() }>
    <CardTitle title={ section.name } subtitle="Card subtitle" />
    <CardText>
      <Items {...props}/>
    </CardText>
  </Card>
))

var titleProps = {
  title: "Menu"
}

class BusinessMenuComponent extends Component {

  constructor() {
    super();

    this.state = {
      showRemoveItemCheck: false,
      menu: {
        name: "loading",
        items: []
      },
      itemCheck: null
    }

  }

  componentDidMount() {

    var hash = window.location.hash;

    var split = hash.split("/")

    console.log(split[2])

    var {business} = this.props;

    var {menus} = business;

    var menu = menus.filter((menu) => menu._id == split[2])[0]
    console.log('menu', menu)

    this.props.getMenuItems({
      mid: split[2]
    })


  }

  updateStateItem(event) {

    console.log(event, "update", this.state)

    var {item, index, key, value} = event

    var itemUpdate = {
      ...item,
      [key]: value
    }

    var items = this.state.menu.items.reduce((items, item, i) => {

      if (i == index) {

        return [...items, itemUpdate]

      } else {

        return [...items, item]

      }

    }, [])

    this.setState({
      menu: {
        ...this.state.menu,
        items
      }
    })

  }


  getCurrentBusiness() {

    var businessInfo = JSON.parse(localStorage.getItem("orders.workchew.admin.business"))

    if (businessInfo && businessInfo._id) {

      return businessInfo

    }

  }


  hideRemoveItemCheck = () => {

    this.setState({

      showRemoveItemCheck: false,
      itemCheck: null

    })

  }

  showRemoveItemCheck = (item) => {

    this.setState({

      showRemoveItemCheck: true,
      itemCheck: item
    })
  }


  render() {
    var removeItemActions = [
      <RemoveItemButton {...{ ...this.props, clearState: ()=> { this.clearState() }, itemCheck: this.state.itemCheck, hideRemoveItemCheck: () => { this.hideRemoveItemCheck() } }} />,
      <CloseRemoveItemButton {...{ ...{ hideRemoveItemCheck: ()=> { this.hideRemoveItemCheck() } } }} />

    ]
    var businessInfo = this.getCurrentBusiness()

    var user = this.props.user;


    var hash = window.location.hash;

    var split = hash.split("/")

    console.log("see split", split[2], this.props)

    var {business} = this.props;

    var {menus} = business;

    var menu = menus.filter((menu) => menu._id == split[2])[0]
    return (
      <div className="h-100 d-flex flex-column">
        { this.state.itemCheck
          && <Dialog title="Remove Item" modal={ false } open={ this.state.showRemoveItemCheck } onRequestClose={ (event) => this.hideRemoveItemCheck() }>
               <p>
                 Are you sure you want to remove
                 { '  ' + this.state.itemCheck.name + " ?" }
               </p>
               <br/>
               { removeItemActions }
             </Dialog> }
        { businessInfo && menu && <Title {...{ title: `${businessInfo.name} Menu/Section/${menu.name}` }} /> }
        <br/>
        <RaisedButton onClick={ (event) => this.props.showAddItemModal({}) }>
          Add Item
        </RaisedButton>
        <br/>
        <div className="flex-1 w-100 scroll-y">
          { menu && menu.items && <Items updateStateItem={ (event) => this.updateStateItem(event) } { ...{ ...menu, ...this.props, showRemoveItemCheck: (item)=> { this.showRemoveItemCheck(item) } }}/> }
        </div>
      </div>
      );
  }

}

const mapBusinessMenuStateToProps = (state, ownProps) => ({
  user: state.user,
  business: state.business,
  routing: state.routing,
})

const BusinessMenu = connect(mapBusinessMenuStateToProps, {
  getMenuItems: ItemActions.getMenuItems,
  selectItemForCart,
  showAddItemModal,
  putMenuItem: ItemActions.putMenuItem,
  deleteMenuItem: ItemActions.deleteMenuItem
})(BusinessMenuComponent)

export default BusinessMenu