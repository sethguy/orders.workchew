import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux'

import { FlexTable, Title, MatFlexTable } from '../../shared/'

import * as businessActions from '../actions'

import * as ItemActions from '../actions/itemsApi'


import RaisedButton from 'material-ui/RaisedButton';
import { Card, CardActions, CardHeader, CardMedia, CardTitle, CardText } from 'material-ui/Card';

import { v4 } from 'uuid'

var {selectItemForCart, showAddItemModal} = businessActions

const Items = (props) => props.items.map((item) => (
  <Card className=" m-1" key={ v4() }>
    <CardTitle title={ item.name } subtitle={ item.price } />
    <CardText>
    </CardText>
    <CardActions>
      <RaisedButton onClick={ (event) => {
                                console.log("menu . items .props", props)
                                props.selectItemForCart(item)
                              } }>
        Add to Cart
      </RaisedButton>
      <RaisedButton onClick={ (event) => {
                              } }>
        remove Item
      </RaisedButton>
    </CardActions>
  </Card>
))

const Sections = (props) => props.sections.map((section) => (

  <Card key={ v4() }>
    <CardTitle title={ section.name } subtitle="Card subtitle" />
    <CardText>
      <Items {...props}/>
    </CardText>
  </Card>
))

var titleProps = {
  title: "Menu"
}

class BusinessMenuComponent extends Component {

  constructor() {
    super();

    this.state = {
      menu: {
        name: "loading",
        items: []
      }
    }

  }

  componentDidMount() {

    var hash = window.location.hash;

    var split = hash.split("/")

    console.log(split[2])

    var {business} = this.props;

    var {menus} = business;


    var menu = menus.filter((menu) => menu._id == split[2])[0]
    console.log('menu', menu)

    this.props.getMenuItems({
      mid: split[2]
    })


    this.setState({
      menu
    })


  }

  render() {
    return (
      <div className="h-100 d-flex flex-column align-items-center">
        <Title {...{ title: `Menu / ${this.state.menu.name}` }} />
        <br/>
        <div className="d-flex flex-1 w-100 flex - wrap scroll-y justify-content-center align-items-center">
          <RaisedButton onClick={ (event) => {
                                  
                                    this.props.showAddItemModal({})
                                  } }>
            Add Item
          </RaisedButton>
          { this.state.menu.items && <Items { ...{ ...this.state.menu, ...this.props }}/> }
        </div>
      </div>
      );
  }

}

const mapBusinessMenuStateToProps = (state, ownProps) => ({
  user: state.user,
  business: state.business,
  routing: state.routing,
})

const BusinessMenu = connect(mapBusinessMenuStateToProps, {
  getMenuItems: ItemActions.getMenuItems,
  selectItemForCart,
  showAddItemModal
})(BusinessMenuComponent)

export default BusinessMenu