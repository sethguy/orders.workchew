import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux'

import { FlexTable, Title, MatFlexTable } from '../../shared/'

import RaisedButton from 'material-ui/RaisedButton';

import { Card, CardActions, CardHeader, CardMedia, CardTitle, CardText } from 'material-ui/Card';

import { v4 } from 'uuid'

import * as ItemActions from '../actions/itemsApi'


import CurrencyFormat from 'react-currency-format';


import * as businessActions from '../actions'
var {selectItemForCart, showAddItemModal} = businessActions

const Items = (props) => props.items.map((item) => {

  var CardId = `item-card-${v4()}`

  return (

    <Card className="w-50 item-cart-bounce" key={ CardId }>
      <CardTitle className="Brandon_bld" title={ item.name } subtitle={ item.description } />
      <CardText>
        <CurrencyFormat value={ item.price } displayType={ 'text' } thousandSeparator={ true } prefix={ '$' } />
      </CardText>
      <CardActions>
        { props.user
          && (props.user.role == "coChewer")
          && <RaisedButton id={ CardId } onClick={ (event) => {
                                                
                                                  console.log("menu . items .props", props)
                                                
                                                  console.log("#" + CardId)
                                                
                                                  window.$("#" + CardId).addClass('animated bounce');
                                                
                                                  window.$('#cart-button').addClass('animated wobble');
                                                
                                                  setTimeout(() => props.selectItemForCart(item), 500)
                                                
                                                } }>
               <span className="Brandon_bld">Add to Cart</span>
             </RaisedButton> }
      </CardActions>
    </Card>
  )

})

class MenuCardComponent extends Component {

  constructor() {
    super();

  }

  componentDidMount() {

    var events = "animationend oAnimationEnd mozAnimationEnd webkitAnimationEnd"

    window.$('.item-cart-bounce').on(events, () => {
      window.$('.item-cart-bounce').removeClass('animated bounce')
    });
  }

  render() {

    var {items} = this.props;

    return (
      <Card className="m-1">
        <div className="w-100 d-flex align-items-center">
          <CardTitle title={ <span style={ { fontSize: 30, color: 'orange' } } className="Brandon_bld"><u>{ this.props.name }</u></span> } />
          { this.props.user
            && (this.props.user.role == "admin" || this.props.user.role == "partner")
            && <RaisedButton className="m-1 Brandon_bld" onClick={ (event) => window.location.hash = `view-menu/${this.props._id}` }>
                 <span className="Brandon_bld">Edit Section</span>
               </RaisedButton> }
          { this.props.user
            && (this.props.user.role == "admin" || this.props.user.role == "partner")
            
            && <RaisedButton className="m-1 Brandon_bld" onClick={ (event) => {
                                                                  this.props.showNameModal(this.props)
                                                                } }>
                 <span className="Brandon_bld">Change Name</span>
               </RaisedButton> }
          { this.props.user
            && (this.props.user.role == "admin" || this.props.user.role == "partner")
            
            && <RaisedButton className="m-1 Brandon_bld" onClick={ (event) => {
                                                                  this.props.showRemoveCheck(this.props)
                                                                } }>
                 <span className="Brandon_bld">Remove Section</span>
               </RaisedButton> }
        </div>
        <CardText className="d-flex flex-wrap">
          { items && <Items {...{ items, ...this.props }}/> }
        </CardText>
      </Card>
      );
  }

}

const mapMenuCardStateToProps = (state, ownProps) => ({
  user: state.user,
  business: state.business
})

const MenuCard = connect(mapMenuCardStateToProps, {
  getMenuItems: ItemActions.getMenuItems,
  selectItemForCart,
  showAddItemModal
})(MenuCardComponent)

export default MenuCard