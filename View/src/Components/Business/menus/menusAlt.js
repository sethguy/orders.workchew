import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux'


import { FlexTable, Title, MatFlexTable } from '../../shared/'

import RaisedButton from 'material-ui/RaisedButton';
import { Card, CardActions, CardHeader, CardMedia, CardTitle, CardText } from 'material-ui/Card';


import { getMenus } from "../actions/api"

import { v4 } from 'uuid'


const Menus = (props) => props.menus.map((menu) => (
  <Card className="m-1 w-25" key={ v4() }>
    <CardTitle title={ menu.name } subtitle={ menu.name } />
    <CardText>
    </CardText>
    <CardActions>
      <RaisedButton onClick={ (event) => {
                                window.location.hash = `view-menu/${menu._id}`
                              
                              } }>
        <span>{ '  ' }</span> Go to
        <span>{ '  ' }</span>
        { menu.name }
        <span>{ '  ' }</span>
      </RaisedButton>
    </CardActions>
  </Card>
))

var titleProps = {
  title: "Menus"
}

class BusinessMenusComponent extends Component {

  constructor() {
    super();
  }

  componentDidMount() {

    this.props.getMenus({
      //bid: 1
    })

  }

  render() {

    console.log('this.props.businessthis.props.businessthis.props.businessthis.props.business', this.props.business)
    return (
      <div className="h-100 d-flex flex-column align-items-center">
        <Title {...titleProps} />
        <br/>
        <div className="d-flex flex-1 w-100 flex-wrap justify-content-center align-items-center">
          { this.props.business.menus && <Menus {...this.props.business}/> }
        </div>
      </div>
      );
  }

}

const mapBusinessMenusStateToProps = (state, ownProps) => ({
  user: state.user,
  business: state.business
})

const BusinessMenus = connect(mapBusinessMenusStateToProps, {
  getMenus
})(BusinessMenusComponent)

export default BusinessMenus