import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux'

import { FlexTable, Title, MatFlexTable } from '../../shared/'

import RaisedButton from 'material-ui/RaisedButton';
import { Card, CardActions, CardHeader, CardMedia, CardTitle, CardText } from 'material-ui/Card';

import { getMenus, putMenu, deleteMenu } from "../actions/api"

import { v4 } from 'uuid'
import * as ItemActions from '../actions/itemsApi'

import MenuCard from '../menu/MenuCard'

import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';

import Dialog from 'material-ui/Dialog';

import TextField from 'material-ui/TextField';

const Menus = (props) => {

  var menuProps = {

    showNameModal: (menuInfo) => {

      props.showNameModal(menuInfo)

    },
    showRemoveCheck: (menuInfo) => {

      props.showRemoveCheck(menuInfo)

    }

  }

  return props.menus.map((menu) => (
    <MenuCard key={ v4() } { ...{ ...menuProps, ...menu }}/>
  ))

}

const AddMenuButton = (props) => {

  var businessInfo = JSON.parse(localStorage.getItem("orders.workchew.admin.business"))

  if (businessInfo._id) {

    return (
      <RaisedButton className="m-1" onClick={ (event) => {
                                        
                                          console.log(props)
                                        
                                          props.putMenu({
                                            name: props.newMenuName,
                                            _id: props.menuSectionInfo._id
                                          })
                                        
                                          props.clearState()
                                        
                                        } }>
        Update Menu Section Name
      </RaisedButton>
    )

  } else {

    return (<span></span>)

  }

}
const CloseAddMenuModalButton = (props) => (
  <RaisedButton className="m-1" onClick={ (event) => {
                                        
                                          props.hideEditMenuSectionNameModal()
                                        
                                        } }>
    Close
  </RaisedButton>
)


const RemoveSectionButton = (props) => {

  var businessInfo = JSON.parse(localStorage.getItem("orders.workchew.admin.business"))

  if (businessInfo._id) {

    return (
      <RaisedButton className="m-1" onClick={ (event) => {
                                        
                                          console.log(props)
                                        
                                          props.deleteMenu({
                                        
                                            _id: props.menuSectionInfo._id
                                          })
                                        
                                          props.clearState()
                                        
                                        } }>
        Remove Menu Section
      </RaisedButton>
    )

  } else {

    return (<span></span>)

  }

}
const CloseRemoveSectionButton = (props) => (
  <RaisedButton className="m-1" onClick={ (event) => {
                                        
                                          props.hideRemoveCheck()
                                        
                                        } }>
    Close
  </RaisedButton>
)


class BusinessMenusComponent extends Component {

  constructor() {
    super();

    this.state = {
      queryParams: {},
      showNamePopUp: false,
      menuInfo: {},
      showRemoveCheck: false

    }
  }

  setCurrentBusiness(businessInfo) {

    if (businessInfo && businessInfo._id) {

      localStorage.setItem("orders.workchew.admin.business", JSON.stringify(businessInfo))

    }

  }

  clearState = () => {

    this.setState({
      showNamePopUp: false,
      menuInfo: {},
      newMenuName: null,
      showRemoveCheck: false,
    })

  }

  componentDidMount() {

    var url = window.location.href;
    console.log('url', url)
    if (url.indexOf('?') > -1) {

      var queryString = url.substring(url.indexOf('?') + 1)

      if (queryString && queryString.length) {

        var splits = queryString.split('&')

        var queryParams = splits

          .map(split => split.split('='))

          .map(([name, value]) => {

            return {

              [name]: value
            }

          })

          .reduce((params, splitItem) => {

            return {
              ...params,
              ...splitItem
            }

          }, {})

        this.setState({
          queryParams
        }, () => {

          if (queryParams.bid) this.props.getMenus({
              bid: queryParams.bid
            })

        })

      }
    }

  }

  hideEditMenuSectionNameModal = () => {

    this.setState({

      showNamePopUp: false,
      menuInfo: {},
      newMenuName: null

    })

  }


  hideRemoveCheck = () => {

    this.setState({

      showRemoveCheck: false,
      menuInfo: {},
      newMenuName: null

    })

  }




  render() {

    var titleProps = {
      title: "Menu"
    }

    var user = this.props.user;

    if (user) {

      var {business} = user;

      if (business) {

        var select = business.reduce((select, run) => {

          if (run._id === this.state.queryParams.bid) {

            return run

          } else {

            return select

          }

        }, {})

        this.setCurrentBusiness(select)

        titleProps = {
          title: select.name + " Menu"
        }

      }

    }

    var businessInfo = {
      ...this.props.business,
      showNameModal: (menuInfo) => {

        this.setState({

          showNamePopUp: true,
          menuInfo: menuInfo,
          newMenuName: null

        })

      },
      showRemoveCheck: (menuInfo) => {

        this.setState({

          showRemoveCheck: true,
          menuInfo: menuInfo,

        })
      }

    }

    var menuSectionNameUpdate = this.state.newMenuName || this.state.menuInfo.name

    var actions = [
      <AddMenuButton {...{ ...this.props, clearState: ()=> { this.clearState() }, newMenuName: menuSectionNameUpdate, menuSectionInfo: this.state.menuInfo }} />,
      <CloseAddMenuModalButton {...{ ...{ hideEditMenuSectionNameModal: ()=> { this.hideEditMenuSectionNameModal() } } }} />
    ]

    var removeSectionActions = [
      <RemoveSectionButton {...{ ...this.props, clearState: ()=> { this.clearState() }, menuSectionInfo: this.state.menuInfo }} />,
      <CloseRemoveSectionButton {...{ ...{ hideRemoveCheck: ()=> { this.hideRemoveCheck() } } }} />

    ]

    return (
      <div className="h-100 d-flex flex-column align-items-center">
        { this.state.menuInfo
          && <Dialog title="Edit Section Name" modal={ false } open={ this.state.showNamePopUp } onRequestClose={ (event) => this.hideEditMenuSectionNameModal() }>
               <TextField onChange={ (event) => this.setState({
                                       newMenuName: event.target.value
                                     }) } hintText="Enter Section Name" value={ menuSectionNameUpdate } floatingLabelText="Enter Section Name" />
               <br/>
               { actions }
             </Dialog> }
        { this.state.menuInfo
          && <Dialog title="Remove Section" modal={ false } open={ this.state.showRemoveCheck } onRequestClose={ (event) => this.hideRemoveCheck() }>
               <p>
                 Are you sure you want to remove
                 { '  ' + this.state.menuInfo.name + " ?" }
               </p>
               <br/>
               { removeSectionActions }
             </Dialog> }
        <Title {...titleProps} />
        <div className="w-100 flex-1 scroll-y">
          { this.props.business.menus && <Menus {...businessInfo } /> }
        </div>
      </div>
      );
  }

}

const mapBusinessMenusStateToProps = (state, ownProps) => ({
  user: state.user,
  business: state.business
})

const BusinessMenus = connect(mapBusinessMenusStateToProps, {
  getMenus,
  putMenu,
  getMenuItems: ItemActions.getMenuItems,
  deleteMenu
})(BusinessMenusComponent)

export default BusinessMenus