
const SELECT_BUSINESS = "SELECT_BUSINESS"

const selectBusinessActionCreater = (payload) => {

  return {
    type: SELECT_BUSINESS,
    payload
  }

}

export const selectBusiness = (payload) => (dispatch, getState) => {

  return dispatch(selectBusinessActionCreater(payload))
}


const SELECT_ITEM_FOR_CART = "SELECT_ITEM_FOR_CART"

const selectItemForCartActionCreater = (payload) => {

  return {
    type: SELECT_ITEM_FOR_CART,
    payload
  }

}

export const selectItemForCart = (payload) => (dispatch, getState) => {

  return dispatch(selectItemForCartActionCreater(payload))
}


const REMOVE_ITEM_FROM_CART = "REMOVE_ITEM_FROM_CART"

const removeItemFromCartActionCreater = (payload) => {

  return {
    type: REMOVE_ITEM_FROM_CART,
    payload
  }

}

export const removeItemFromCart = (payload) => (dispatch, getState) => {

  return dispatch(removeItemFromCartActionCreater(payload))
}



const SHOW_ADD_MENU_MODAL = "SHOW_ADD_MENU_MODAL"

const showAddMenuModalActionCreater = (payload) => {

  return {
    type: SHOW_ADD_MENU_MODAL,
    payload
  }

}

export const showAddMenuModal = (payload) => (dispatch, getState) => {

  return dispatch(showAddMenuModalActionCreater(payload))
}



const HIDE_ADD_MENU_MODAL = "HIDE_ADD_MENU_MODAL"

const hideAddMenuModalActionCreater = (payload) => {

  return {
    type: HIDE_ADD_MENU_MODAL,
    payload
  }

}

export const hideAddMenuModal = (payload) => (dispatch, getState) => {

  return dispatch(hideAddMenuModalActionCreater(payload))
}





const SHOW_ADD_ITEM_MODAL = "SHOW_ADD_ITEM_MODAL"

const showAddItemModalActionCreater = (payload) => {

  return {
    type: SHOW_ADD_ITEM_MODAL,
    payload
  }

}

export const showAddItemModal = (payload) => (dispatch, getState) => {

  return dispatch(showAddItemModalActionCreater(payload))
}



const HIDE_ADD_ITEM_MODAL = "HIDE_ADD_ITEM_MODAL"

const hideAddItemModalActionCreater = (payload) => {

  return {
    type: HIDE_ADD_ITEM_MODAL,
    payload
  }

}

export const hideAddItemModal = (payload) => (dispatch, getState) => {

  return dispatch(hideAddItemModalActionCreater(payload))
}

export const BusinessActionTypes = {
  SELECT_BUSINESS,
  SELECT_ITEM_FOR_CART,
  REMOVE_ITEM_FROM_CART,
  SHOW_ADD_MENU_MODAL,
  HIDE_ADD_MENU_MODAL,
  SHOW_ADD_ITEM_MODAL,
  HIDE_ADD_ITEM_MODAL
}