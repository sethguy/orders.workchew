import { CALL_API, Schemas } from '../../../middleware/api'


export const GET_BUSINESS_MENUS = 'GET_BUSINESS_MENUS'
export const GET_BUSINESS_MENUS_SUCCESS = 'GET_BUSINESS_MENUS_SUCCESS'
export const GET_BUSINESS_MENUS_FAILURE = 'GET_BUSINESS_MENUS_FAILURE'

const businessMenuPath = "/menus"

const getMenusActionCreator = (query) => ({
  [CALL_API]: {
    types: [GET_BUSINESS_MENUS, GET_BUSINESS_MENUS_SUCCESS, GET_BUSINESS_MENUS_FAILURE],
    endpoint: `${businessMenuPath}?bid=${query.bid}`,
    config: {

      method: "GET",

      headers: {
        "Content-type": "application/json"
      },

    }
  }
})

export const getMenus = (query) => (dispatch, getState) => {

  return dispatch(getMenusActionCreator(query))
}

export const POST_BUSINESS_MENU = 'POST_BUSINESS_MENU'
export const POST_BUSINESS_MENU_SUCCESS = 'POST_BUSINESS_MENU_SUCCESS'
export const POST_BUSINESS_MENU_FAILURE = 'POST_BUSINESS_MENU_FAILURE'

const postMenuActionCreator = (body, token) => ({
  [CALL_API]: {
    types: [POST_BUSINESS_MENU, POST_BUSINESS_MENU_SUCCESS, POST_BUSINESS_MENU_FAILURE],
    endpoint: `${businessMenuPath}`,
    config: {

      method: "POST",

      headers: {
        "Content-type": "application/json",
        "x-api-access-token": token
      },

      body: JSON.stringify(body)

    }
  }
})

export const postMenu = (userInfo) => (dispatch, getState) => {
  var {user} = getState();

  var {token} = user;

  return dispatch(postMenuActionCreator(userInfo, token))
}

export const PUT_BUSINESS_MENU = 'PUT_BUSINESS_MENU'
export const PUT_BUSINESS_MENU_SUCCESS = 'PUT_BUSINESS_MENU_SUCCESS'
export const PUT_BUSINESS_MENU_FAILURE = 'PUT_BUSINESS_MENU_FAILURE'

const putMenuActionCreator = (body, token) => ({
  [CALL_API]: {
    types: [PUT_BUSINESS_MENU, PUT_BUSINESS_MENU_SUCCESS, PUT_BUSINESS_MENU_FAILURE],
    endpoint: `${businessMenuPath}`,
    config: {

      method: "PUT",

      headers: {
        "Content-type": "application/json",
        "x-api-access-token": token
      },

      body: JSON.stringify(body)

    }
  }
})

export const putMenu = (userInfo) => (dispatch, getState) => {
  var {user} = getState();

  var {token} = user;
  return dispatch(putMenuActionCreator(userInfo, token))
}


export const DELETE_BUSINESS_MENU = 'DELETE_BUSINESS_MENU'
export const DELETE_BUSINESS_MENU_SUCCESS = 'DELETE_BUSINESS_MENU_SUCCESS'
export const DELETE_BUSINESS_MENU_FAILURE = 'DELETE_BUSINESS_MENU_FAILURE'

const deleteMenuActionCreator = (body, token) => ({
  [CALL_API]: {
    types: [DELETE_BUSINESS_MENU, DELETE_BUSINESS_MENU_SUCCESS, DELETE_BUSINESS_MENU_FAILURE],
    endpoint: `${businessMenuPath}`,
    config: {

      method: "DELETE",

      headers: {
        "Content-type": "application/json",
        "x-api-access-token": token
      },

      body: JSON.stringify(body)

    }
  }
})

export const deleteMenu = (userInfo) => (dispatch, getState) => {
  var {user} = getState();

  var {token} = user;
  return dispatch(deleteMenuActionCreator(userInfo, token))
}

