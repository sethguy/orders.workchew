import { CALL_API, Schemas } from '../../../middleware/api'


export const GET_MENU_ITEMS = 'GET_MENU_ITEMS'
export const GET_MENU_ITEMS_SUCCESS = 'GET_MENU_ITEMS_SUCCESS'
export const GET_MENU_ITEMS_FAILURE = 'GET_MENU_ITEMS_FAILURE'

const menuItemPath = "/menu-items"

const getMenuItemsActionCreator = (query) => ({
  [CALL_API]: {
    types: [GET_MENU_ITEMS, GET_MENU_ITEMS_SUCCESS, GET_MENU_ITEMS_FAILURE],
    endpoint: `${menuItemPath}?mid=${query.mid}`,
    config: {

      method: "GET",

      headers: {
        "Content-type": "application/json"
      },

    },
    mid: query.mid
  }
})

export const getMenuItems = (itemInfo) => (dispatch, getState) => {

  return dispatch(getMenuItemsActionCreator(itemInfo))
}

export const POST_MENU_ITEM = 'POST_MENU_ITEM'
export const POST_MENU_ITEM_SUCCESS = 'POST_MENU_ITEM_SUCCESS'
export const POST_MENU_ITEM_FAILURE = 'POST_MENU_ITEM_FAILURE'

const postMenuItemActionCreator = (body, token) => ({
  [CALL_API]: {
    types: [POST_MENU_ITEM, POST_MENU_ITEM_SUCCESS, POST_MENU_ITEM_FAILURE],
    endpoint: `${menuItemPath}`,
    config: {

      method: "POST",

      headers: {
        "Content-type": "application/json",
        "x-api-access-token": token

      },

      body: JSON.stringify(body)

    }
  }
})

export const postMenuItem = (itemInfo) => (dispatch, getState) => {
  var {user} = getState();

  var {token} = user;
  return dispatch(postMenuItemActionCreator(itemInfo, token))
}

export const PUT_MENU_ITEM = 'PUT_MENU_ITEM'
export const PUT_MENU_ITEM_SUCCESS = 'PUT_MENU_ITEM_SUCCESS'
export const PUT_MENU_ITEM_FAILURE = 'PUT_MENU_ITEM_FAILURE'

const putMenuItemActionCreator = (body, token) => ({
  [CALL_API]: {
    types: [PUT_MENU_ITEM, PUT_MENU_ITEM_SUCCESS, PUT_MENU_ITEM_FAILURE],
    endpoint: `${menuItemPath}`,
    config: {

      method: "PUT",

      headers: {
        "Content-type": "application/json",
        "x-api-access-token": token

      },

      body: JSON.stringify(body)

    }
  }
})

export const putMenuItem = (itemInfo) => (dispatch, getState) => {
  var {user} = getState();

  var {token} = user;
  return dispatch(putMenuItemActionCreator(itemInfo, token))
}

export const DELETE_MENU_ITEM = 'DELETE_MENU_ITEM'
export const DELETE_MENU_ITEM_SUCCESS = 'DELETE_MENU_ITEM_SUCCESS'
export const DELETE_MENU_ITEM_FAILURE = 'DELETE_MENU_ITEM_FAILURE'

const deleteMenuItemActionCreator = (body, token) => ({
  [CALL_API]: {
    types: [DELETE_MENU_ITEM, DELETE_MENU_ITEM_SUCCESS, DELETE_MENU_ITEM_FAILURE],
    endpoint: `${menuItemPath}`,
    config: {

      method: "DELETE",

      headers: {
        "Content-type": "application/json",
        "x-api-access-token": token

      },

      body: JSON.stringify(body)

    }
  }
})

export const deleteMenuItem = (itemInfo) => (dispatch, getState) => {
  var {user} = getState();

  var {token} = user;
  return dispatch(deleteMenuItemActionCreator(itemInfo, token))
}