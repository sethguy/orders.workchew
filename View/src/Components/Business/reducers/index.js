import * as businessActions from '../actions'

var getCartTotal = (cart) => {

  var {items} = cart;

  return items.reduce((total, item) => {
    console.log('calc', total, item.price)
    return parseFloat(total) + parseFloat(item.price)

  }, 0)

}

export const business = (state = {
    showAddMenuModal: false,
    showAddItemModal: false,
    cart: {
      items: []
    }
  } , action) => {
  console.log("@ business reducer", state, action)
  //GET_BUSINESS_MENUS_SUCCESS
  switch (action.type) {

    case "GET_ORDERS_SUCCESS": {

      return {
        ...state,
        receipt: action.response

      }
    }
    case "POST_ORDER_SUCCESS": {

      return {
        ...state,
        cart: {
          items: [],
          total: 0
        }

      }
    }
    case "POST_MENU_ITEM_SUCCESS": {

      console.log("@ business POST_MENU_ITEM_SUCCESS", state, action)

      var newItem = action.response.postMenuitemsResponse

      var menus = state.menus.reduce((menus, menu) => {

        if (menu._id == newItem.mid) {

          if (!menu.items) {
            menu.items = []
          }

          menu.items.push(newItem)

        }

        return [...menus, menu]
      }, [])

      return {
        ...state,
        showAddItemModal: false,
        menus
      }
    }
    case "POST_BUSINESS_MENU_SUCCESS": {
      return {
        ...state,

        menus: [...state.menus, action.response.postMenusResponse],
        showAddMenuModal: false
      }
    }
    case "GET_MENU_ITEMS_SUCCESS": {

      var newItems = Object.keys(action.response).map(key => action.response[key])

      console.log(" GET_MENU_ITEMS_SUCCESS", newItems)

      if (newItems) {

        var mid = action.mid

        var menus = state.menus.reduce((menus, menu) => {

          if (menu._id == mid) {

            if (!menu.items) {
              menu.items = []
            }

            menu.items = [...newItems]
          }

          return [...menus, menu]
        }, [])

        return {
          ...state,
          menus
        }

      } else {

        return {
          ...state,
        }

      }

    }
    case "GET_BUSINESS_MENUS_SUCCESS": {

      console.log('GET_BUSINESS_MENUS_SUCCESS', action.response)
      return {
        ...state,
        menus: [...Object.keys(action.response).map(key => action.response[key])],
      }
    }
    case businessActions.BusinessActionTypes.SHOW_ADD_ITEM_MODAL: {

      var {payload} = action;

      return {
        ...state,
        showAddItemModal: true
      }

    }
    case businessActions.BusinessActionTypes.HIDE_ADD_ITEM_MODAL: {

      var {payload} = action;

      return {
        ...state,
        showAddItemModal: false
      }

    }
    case businessActions.BusinessActionTypes.SHOW_ADD_MENU_MODAL: {

      var {payload} = action;

      return {
        ...state,
        showAddMenuModal: true
      }

    }
    case businessActions.BusinessActionTypes.HIDE_ADD_MENU_MODAL: {

      var {payload} = action;

      return {
        ...state,
        showAddMenuModal: false
      }

    }
    case businessActions.BusinessActionTypes.SELECT_ITEM_FOR_CART: {

      var {cart} = state;

      var {payload} = action;

      var items = [...cart.items, {
        ...payload
      }]

      var cartUpdate = {
        ...cart,
        items,
        total: getCartTotal({
          items
        })

      }

      return {
        ...state,
        cart: cartUpdate,
      }

    }
    case businessActions.BusinessActionTypes.REMOVE_ITEM_FROM_CART: {

      var {cart} = state;

      var {payload} = action;

      var items = [...cart.items];
      console.log("@ business reducer items ", items)

      items.splice(payload.index, 1)

      var cartUpdate = {
        ...cart,
        items,
        total: getCartTotal({
          items
        })
      }

      return {
        ...state,
        cart: cartUpdate
      }

    }
    default:
      return state;
  }

}