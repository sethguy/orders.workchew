import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux'

import './style.css';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';


class AppComponent extends Component {
  constructor() {
    super();
  }
  render() {
    return (
      <div className="app-component">
      </div>
      );
  }
}

const mapAppStateToProps = (state, ownProps) => ({
  user: state.user
})

const App = connect(mapAppStateToProps, {


})(AppComponent)

export default App