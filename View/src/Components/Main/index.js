import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux'
import { FlexTable, Title, MatFlexTable } from '../shared/'

import * as Actions from '../../actions'
import RaisedButton from 'material-ui/RaisedButton';
import { Card, CardActions, CardHeader, CardMedia, CardTitle, CardText } from 'material-ui/Card';

import { v4 } from 'uuid'

const PartnerItems = (props) => props.partners.map((partner, i) => (

  <Card className="m-1" style={ { height: 200, width: 200 } } key={ v4() }>
    <CardTitle title={ partner.name } />
    <CardText>
    </CardText>
    <CardActions>
      <RaisedButton onClick={ (event) => {
                                localStorage.setItem("orders.workchew.admin.business", JSON.stringify(partner))
                              
                                window.location.hash = "business-menus?bid=" + partner._id
                              
                              } }>
        Go To Menu
      </RaisedButton>
    </CardActions>
  </Card>


))

class AppComponent extends Component {
  constructor() {
    super();

    this.state = {
      queryParams: {}
    }

  }

  componentDidMount() {
    localStorage.setItem("orders.workchew.admin.business", JSON.stringify({}))

    var url = window.location.href;

    console.log('url', url)

    if (url.indexOf('?') > -1) {

      var slashes = url.split('/')

      var lastSlash = slashes[slashes.length - 1]
      console.log('slashes', lastSlash)

      var queryString = url.substring(url.indexOf('?') + 1)

      if (queryString && queryString.length) {

        var splits = queryString.split('&')

        var queryParams = splits

          .map(split => split.split('='))

          .map(([name, value]) => {

            return {

              [name]: value
            }

          })

          .reduce((params, splitItem) => {

            return {
              ...params,
              ...splitItem
            }

          }, {})



        this.setState({
          queryParams
        }, () => {


          if (queryParams.token && queryParams.token.length) this.props.login({

              body: {
                token: queryParams.token,
                bid: queryParams.bid
              }

            })

        })



      }
    } else {




    }
  }

  render() {
    var url = window.location.href;

    console.log('url', url)


    var slashes = url.split('/')

    var lastSlash = slashes[slashes.length - 1]
    console.log('slashes', lastSlash)


    if ((lastSlash.indexOf('dashboard') == -1)
      && this.state.queryParams.bid
      && this.state.queryParams
      && this.props
      && this.props.user
      && this.props.user.token
      && (this.state.queryParams.token == this.props.user.token)) {


      var select = this.props.user.business.reduce((select, run) => {

        if (run._id === this.state.queryParams.bid) {

          return run

        } else {

          return select

        }

      }, {})

      localStorage.setItem("orders.workchew.admin.business", JSON.stringify(select))

      window.location.hash = "business-menus?bid=" + select._id

    }

    return (
      <div className="h-100 d-flex flex-column align-items-center">
        <div className="w-100">
          <Title {...{ title: "Orders @workchew.com" }} />
        </div>
        { this.props.user
          && this.props.user.business
          && <div className="flex-1 scroll-y d-flex flex-wrap w-100">
               <PartnerItems { ...{ partners: this.props.user.business }} />
             </div> }
      </div>
      );
  }
}

const mapAppStateToProps = (state, ownProps) => ({
  user: state.user
})

const App = connect(mapAppStateToProps, {
  login: Actions.login
})(AppComponent)

export default App