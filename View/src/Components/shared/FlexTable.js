import React, { Component } from 'react';

class FlexTable extends Component {

  constructor(props) {

    super(props);

    this.state = this.props

  }

  getTableEntry(item, row) {

    if (this.props.getTableEntry) {

      return this.props.getTableEntry(item, row)

    } else {

      return item[row.key];

    }

  }

  selectItem(item) {

    console.log('flex table item selected', item)

    if (this.props.selectItem) {

      return this.props.selectItem(item)

    } else {


    }

  }

  render() {

    return (
      <table className="flex-table">
        <thead>
          <tr>
            { this.props.tableRows.map((row, i) => (
              
                <th key={ i }>
                  { row.title }
                </th>
              
              )) }
            { this.props.removeItem && <th></th> }
          </tr>
        </thead>
        <tbody>
          { this.props.items &&
            this.props.items
              .map(
                (item, i) => (
            
                  <tr key={ i }>
                    { this.props.tableRows.map((row, j) => (
                      
                        <td key={ j } onClick={ (event) => {
                                              
                                                this.selectItem(item)
                                              
                                              } }>
                          { this.getTableEntry(item, row) }
                        </td> )
                      
                      ) }
                    { this.props.removeItem &&
                      <td>
                        <button onClick={ (event) => {
                                          
                                            this.props.removeItem(event, item)
                                          
                                          } } className='btn btn-warning'>
                          remove
                        </button>
                      </td> }
                  </tr>)
            ) }
        </tbody>
      </table>);
  }

}

export default FlexTable;