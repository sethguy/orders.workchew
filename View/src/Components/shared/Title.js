import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import { connect } from 'react-redux'

import MenuIcon from 'material-ui/svg-icons/navigation/menu';
import ActionCart from 'material-ui/svg-icons/action/add-shopping-cart';

import DeleteForever from 'material-ui/svg-icons/action/delete-forever';

import Menu, { MenuItem } from 'material-ui/Menu';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

import { v4 } from 'uuid'

import CartDialog from '../Business/Cart'

import TextField from 'material-ui/TextField';

import { showAddMenuModal, hideAddMenuModal, hideAddItemModal } from '../Business/actions';

import { postMenu } from '../Business/actions/api';

import { postMenuItem } from '../Business/actions/itemsApi';

import './title.css'

const CartButton = (props) => {

  var {user} = props

  if (user && user.role == 'coChewer') {

    return (
      <IconButton id="cart-button" onClick={ props.handleOpen }>
        <ActionCart />
      </IconButton>
    )

  }

  return (<span></span> )

}

const MenuButton = (props) => {

  var {user} = props;

  var businessInfo = JSON.parse(localStorage.getItem("orders.workchew.admin.business"))

  return (  <IconMenu iconButtonElement={ <IconButton>
                                <MenuIcon />
                              </IconButton> } targetOrigin={ { horizontal: 'right', vertical: 'top' } } anchorOrigin={ { horizontal: 'right', vertical: 'top' } }>
              { businessInfo
                && businessInfo._id
                && user
                && ((user.role == 'admin')
                || (user.role == 'partner'
                && (user.business[0]._id == businessInfo._id)))
                && <MenuItem primaryText="Add Menu Section" onClick={ (event) => {
                                                                   
                                                                     props.showAddMenuModal()
                                                                   
                                                                   } } /> }
              { businessInfo
                && businessInfo._id
                && user
                && ((user.role == 'admin'))
                && <MenuItem primaryText="Go To Dashboard" onClick={ (event) => {
                                                                  
                                                                    window.location.hash = "dashboard?bid=" + businessInfo._id + "&token=" + user.token
                                                                  
                                                                  } } /> }
              { businessInfo
                && businessInfo._id
                && user
                && ((user.role == 'admin'))
                && <MenuItem primaryText="Go To Admin PartnerPage" onClick={ (event) => {
                                                                          
                                                                            window.location.href = "https://app.workchew.com/#/admin-partner-page?id=" + businessInfo._id
                                                                          
                                                                          } } /> }
              { businessInfo
                && businessInfo._id
                && user
                && ((user.role == 'admin')
                || (user.role == 'partner'
                && (user.business[0]._id == businessInfo._id)))
                && <MenuItem primaryText="Go To Main Partner Page" onClick={ (event) => {
                                                                          
                                                                            window.location.href = "https://app.workchew.com/#/partner-page?id=" + businessInfo._id
                                                                          
                                                                          } } /> }
              { businessInfo
                && businessInfo._id
                && user
                && <MenuItem primaryText="Back to Partner Profile" onClick={ (event) => {
                                                                          
                                                                            window.location.href = "https://app.workchew.com/#/business-page?id=" + businessInfo._id
                                                                          
                                                                          } } /> }
              { businessInfo
                && businessInfo._id
                && user
                && <MenuItem primaryText="List of Workchew locations" onClick={ (event) => {
                                                                             
                                                                               window.location.href = "https://app.workchew.com/#/locations"
                                                                             
                                                                             } } /> }
            </IconMenu>)

}

const AddMenuButton = (props) => {

  var businessInfo = JSON.parse(localStorage.getItem("orders.workchew.admin.business"))

  if (businessInfo._id) {

    return (
      <RaisedButton className="m-1" onClick={ (event) => {
                                        
                                          props.postMenu({
                                            name: props.newMenuName,
                                            bid: businessInfo._id
                                          })
                                          props.clearState()
                                        
                                        } }>
        Add Menu Section
      </RaisedButton>
    )

  } else {

    return (<span></span>)

  }

}

const addItem = (props) => {

  props.postMenuItem({
    name: props.newItemName,
    price: props.newItemPrice,
    mid: props.menuId
  })
  props.clearState()

}

const AddItemButton = (props) => (
  <RaisedButton className="m-1" onClick={ (event) => addItem(props) }>
    Add Item
  </RaisedButton>
)

const CloseAddMenuModalButton = (props) => (
  <RaisedButton className="m-1" onClick={ (event) => {
                                        
                                          props.hideAddMenuModal()
                                        
                                        } }>
    Close
  </RaisedButton>
)

const CloseAddItemModalButton = (props) => (
  <RaisedButton className="m-1" onClick={ (event) => {
                                        
                                          props.hideAddItemModal()
                                        
                                        } }>
    Close
  </RaisedButton>
)

//<AppBar iconElementLeft={ } iconElementRight={ <CartButton { ...props}/> } title={ props.title } />
/*<RaisedButton className="m-1" onClick={ (event) => {
                                        
                                          window.history.back()
                                        } }>
  Back
</RaisedButton>*/
const Title = (props) => (


  <div className="d-flex align-items-center" style={ { height: 64, width: '100%', color: 'white', backgroundColor: 'orange' } }>
    <MenuButton {...props} />
    <div style={ { width: 30 } }></div>
    <div style={ { width: 30 } }></div>
    <h2 className="Brandon_bld" style={ { position: 'relative' } }>{ props.title }</h2>
    <div className="flex-1 h-100 d-flex align-items-center justify-content-end">
      <CartButton { ...props}/>
    </div>
  </div>

)

class TitleComponent extends Component {

  constructor(props) {

    super(props);

    this.state = {
      ...this.props,
      open: false,
      newMenuName: "",
      newItemPrice: 0,
      newItemName: "",
      newItemDescription: ""
    }

  }

  handleClose = () => {
    this.setState({
      open: false
    });
  };
  handleOpen = () => {

    this.setState({
      open: true
    });
  };

  clearState = () => {

    this.setState({
      newMenuName: "",
      newItemPrice: 0,
      newItemName: "",
      newItemDescription: ""
    })

  }

  componentDidMount() {

    var hash = window.location.hash;

    var split = hash.split("/")

    console.log(split[2])

    var {business} = this.props;

    var {menus} = business;
    var menu = menus.filter((menu) => menu._id == split[2])[0]
    if (menu) {
      console.log('menu', menu)
      this.setState({
        menuId: menu._id
      })
    }

    var events = "animationend oAnimationEnd mozAnimationEnd webkitAnimationEnd"

    window.$('#cart-button').on(events, () => {
      window.$('#cart-button').removeClass('animated wobble')
    });

  }

  render() {

    var {handleOpen, clearState, handleClose, state: {open, menuId, newItemName, newItemPrice}, props: {business, hideAddMenuModal, hideAddItemModal}} = this

    if (!open) {
      open = false
    }

    var closeProps = {
      hideAddItemModal
    }

    const actions = [<AddMenuButton {...{ ...this.props, clearState, newMenuName: this.state.newMenuName }} />, <CloseAddMenuModalButton {...this.props} />]

    const addItemActions = [<AddItemButton {...{ ...this.props, menuId, clearState, newItemName, newItemPrice }} />, <CloseAddItemModalButton {...closeProps } />]

    return (<div className="w-100">
              <Title {...{ ...this.props, handleOpen, handleClose, open }} />
              <CartDialog {...{ ...this.props, handleOpen, handleClose, open }} />
              <Dialog actions={ actions } title="New Menu Section" modal={ false } open={ business.showAddMenuModal || false } onRequestClose={ (event) => hideAddMenuModal({}) }>
                <TextField onChange={ (event) => this.setState({
                                        newMenuName: event.target.value
                                      }) } hintText="Enter Section Name" value={ this.state.newMenuName } floatingLabelText="Enter Section Name" />
              </Dialog>
              <Dialog actions={ addItemActions } title="New Menu Item" modal={ false } open={ business.showAddItemModal || false } onRequestClose={ (event) => hideAddItemModal({}) }>
                <div className="w-100">
                  <TextField className="w-100" onChange={ (event) => this.setState({
                                                            newItemName: event.target.value
                                                          }) } hintText="Enter Item Name" value={ this.state.newItemName } floatingLabelText="Enter Item Name" />
                  <br/>
                  <TextField className="w-100" onChange={ (event) => this.setState({
                                                            newItemPrice: event.target.value
                                                          }) } hintText="Enter Item Price" value={ this.state.newItemPrice } floatingLabelText="Enter Item Price" />
                  <br/>
                  <TextField className="w-100" onChange={ (event) => this.setState({
                                                            newItemDescription: event.target.value
                                                          }) } hintText="Enter Item Description" value={ this.state.newItemDescription } floatingLabelText="Enter Item Description"
                  />
                  <br/>
                </div>
              </Dialog>
            </div>);
  }

}

const mapTitleBarStateToProps = (state, ownProps) => ({
  user: state.user,
  business: state.business
})

const TitleBar = connect(mapTitleBarStateToProps, {
  showAddMenuModal,
  hideAddMenuModal,
  hideAddItemModal,
  postMenu,
  postMenuItem
})(TitleComponent)

export default TitleBar


