const LOGIN = "LOGIN"

export const ActionTypes = {
  LOGIN
}

const loginAction = (config) => {

  return {
    type: LOGIN,
    config
  }

}

export const login = (config) => (dispatch, getState) => {

  return dispatch(loginAction(config))
}