import React, { Component } from 'react';
import { render } from 'react-dom';
import { Provider, connect } from 'react-redux'
import routes from './routes'

import { store } from './Store';

import App from './Components/App/';

import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import { HashRouter } from 'react-router-dom'
import { syncHistoryWithStore } from 'react-router-redux'

import { createHashHistory } from 'history';

import BusinessMenusComponent from './Components/Business/menus'

import BusinessMenuComponent from './Components/Business/menu'


import Checkout from './Components/Business/Checkout'

import Receipt from './Components/Business/Receipt'

import Main from './Components/Main'


import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';


const history = syncHistoryWithStore(createHashHistory(), store)

const Root = ({store, history}) => (
  <Provider store={ store }>
    <HashRouter history={ history }>
      <MuiThemeProvider>
        <div className="root">
          <Route exact path="/dashboard" component={ Main }></Route>
          <Route exact path="/" component={ Main }></Route>
          <Route exact path="/business-menus" component={ BusinessMenusComponent } />
          <Route exact path="/view-menu/:id" component={ BusinessMenuComponent } />
          <Route exact path="/checkout/" component={ Checkout } />
          <Route exact path="/receipt" component={ Receipt } />
        </div>
      </MuiThemeProvider>
    </HashRouter>
  </Provider>
)

render(<Root store={ store } />, document.getElementById('root'));


