import * as Actions from '../actions';

export const user = (state = {}, action) => {

  switch (action.type) {
    case "LOGIN_SUCCESS":
      return Object.assign({}, state, {
        ...action.data.user,
        business: action.data.business
      });
    default:
      return state;
  }

}
