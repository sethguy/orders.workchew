const db = require('./dbService');
const Rx = require('rxjs');

const _ = require('lodash');

const menuItemsService = require("./menuItemsService")

const menusCollectionName = 'menus'

var post = function(menus) {

  return db

    .post(menusCollectionName, menus)

}

var put = function(menus) {

  return db

    .update(menusCollectionName, menus, {
      _id: menus._id
    })

}

var update = function(menus) {

  var query = {
    _id: menus._id
  }

  return db.get(menusCollectionName, query)

    .switchMap((getResponse) => {

      var menusUpdate = _.extend(getResponse[0], menus)

      return db

        .update(menusCollectionName, menusUpdate, {
          _id: menus._id
        })

    })

}


var remove = function(query) {

  return db.delete(menusCollectionName, query)

}


var getMenuItems = function(menus) {

  var geItems = menus.map((menu) => menuItemsService.get({
    mid: `${menu._id.valueOf()}`
  }))

  return Rx.Observable.forkJoin(geItems)


}

var get = function(query) {

  if (query.searchTerm) {

    query = {
      'name': {
        $regex: ".*" + query.searchTerm + ".*",
        $options: "i"
      }
    }

  }

  return db.get(menusCollectionName, query)



}

var menusService = {

  delete: remove,

  getMenuItems,

  get,

  post,

  update,

  put
}

module.exports = menusService