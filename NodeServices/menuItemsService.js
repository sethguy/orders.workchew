const db = require('./dbService');
const Rx = require('rxjs');

const _ = require('lodash');

const menuItemsCollectionName = 'menuItems'

var post = function(menuItems) {

  return db

    .post(menuItemsCollectionName, menuItems)

}

var put = function(menuItems) {

  return db

    .update(menuItemsCollectionName, menuItems, {
      _id: menuItems._id
    })

}

var update = function(menuItems) {

  var query = {
    _id: menuItems._id
  }

  return db.get(menuItemsCollectionName, query)

    .switchMap((getResponse) => {

      var menuItemsUpdate = _.extend(getResponse[0], menuItems)

      return db

        .update(menuItemsCollectionName, menuItemsUpdate, {
          _id: menuItems._id
        })

    })

}


var remove = function(query) {

  return db.delete(menuItemsCollectionName, query)

}

var get = function(query) {

  if (query.searchTerm) {

    query = {
      'name': {
        $regex: ".*" + query.searchTerm + ".*",
        $options: "i"
      }
    }

  }

  return db.get(menuItemsCollectionName, query)

}

var menuItemsService = {

  delete: remove,

  get,

  post,

  update,

  put
}

module.exports = menuItemsService