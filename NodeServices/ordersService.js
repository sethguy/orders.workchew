const db = require('./dbService');
const Rx = require('rxjs');

const payPal = require('./pay-pal')

const _ = require('lodash');

const ordersCollectionName = 'orders'

var post = function(orders) {

  return db

    .post(ordersCollectionName, orders)

}

var put = function(orders) {

  return db

    .update(ordersCollectionName, orders, {
      _id: orders._id
    })

}

var update = function(orders) {

  var query = {
    _id: orders._id
  }

  return db.get(ordersCollectionName, query)

    .switchMap((getResponse) => {

      var ordersUpdate = _.extend(getResponse[0], orders)

      return db

        .update(ordersCollectionName, ordersUpdate, {
          _id: orders._id
        })

    })

}


var remove = function(query) {

  return db.delete(ordersCollectionName, query)

}

var get = function(query) {

  // if (query.searchTerm) {

  //   query = {
  //     'name': {
  //       $regex: ".*" + query.searchTerm + ".*",
  //       $options: "i"
  //     }
  //   }

  // }

  // return db.get(ordersCollectionName, query)

  return payPal.getPayment(query.payment)

}

var ordersService = {

  delete: remove,

  get,

  post,

  update,

  put
}

module.exports = ordersService