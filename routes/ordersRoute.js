const Rx = require('rxjs')

const express = require('express');

const router = express.Router();

const orders = require('../NodeServices/ordersService.js')

router.post('/', (req, res) => {

  console.log('req.body @ orders post route', req.body)

  orders

    .post(req.body)

    .subscribe((postOrdersResponse) => {

      res.send({
        postOrdersResponse
      })

    })

}); //POST 

router.put('/', (req, res) => {

  console.log('req.body @ orders put route', req.body)

  orders

    .update(req.body)

    .subscribe((putOrdersResponse) => {

      res.send({
        putOrdersResponse
      })

    })

}) //PUT

router.get('/', (req, res) => {

  return orders

    .get(req.query)

    .subscribe((getOrdersResponse) => {

      res.send(getOrdersResponse)

    })

})

router.delete('/', (req, res) => {

  return orders

    .delete(req.query)

    .subscribe((getOrdersResponse) => {

      res.send(getOrdersResponse)

    })

})


module.exports = router;