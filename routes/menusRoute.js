const Rx = require('rxjs')

const express = require('express');

const router = express.Router();

const menus = require('../NodeServices/menusService.js')

const request = require('request');

var checkAccessToken = function(req, res, next) {

  var token = req.headers['x-api-access-token'];

  var {_id, bid} = req.body;

  menuCheck(_id, bid)

    .subscribe((menu) => {

      var {bid} = menu;

      if (bid && bid.length) {

        verifyUser(token, bid)

          .subscribe((ordersLoginResponse) => {

            if (ordersLoginResponse.user.role == "coChewer") {

              res.status(401).send({
                error: 401,
                msg: "not authorized"
              })

            } else {

              next()

            }

          })

      }

    })

} //checkAccessToken

var menuCheck = function(_id, bid) {

  console.log(_id, bid)

  if (_id && _id.length) {

    return getMenu(_id)

  }

  if (!_id && bid && bid.length) {

    return Rx.Observable.of({
      bid
    })

  }

}

var getMenu = function(mid) {

  return menus

    .get({
      _id: mid
    })

    .map((items) => {
      return items[0]
    })

} //getItemAndMenu

var verifyUser = function(token, bid) {

  return Rx.Observable.create(function(observer) {

    request.post({
      url: 'https://app.workchew.com/orderslogin',

      body: JSON.stringify({
        token: token,
        bid: bid

      }),

      headers: {

        "Content-Type": "application/json"

      }

    }, function(err, httpResponse, body) {

      observer.next(JSON.parse(body))

    })

  })

} //verifyUser

var MenusAuth = function(req, res, next) {

  var body = req.body;
  var query = req.query;
  var time = new Date().toUTCString();
  var headers = req.headers;
  var method = req.method;
  var params = req.params;
  var route = req.route;
  var files = req.files;
  var cookies = req.cookies;
  var signedCookies = req.signedCookies;
  var url = req.url;

  var accessToken = headers['x-api-access-token']
  if (method.toLowerCase() === "post"
    || method.toLowerCase() === "put"
    || method.toLowerCase() === "delete") {

    onUpdateMehtod(req, res, next)

  } else {

    next()

  }

} //MenusAuth

router.use(MenusAuth)

var onUpdateMehtod = function(req, res, next) {

  if (req.headers['x-api-access-token'] && req.headers['x-api-access-token'].length) {

    checkAccessToken(req, res, next)

  } else {

    res.status(401).send({
      error: 401,
      msg: "no access token"
    })

  }

}


router.post('/', (req, res) => {

  console.log('req.body @ menus post route', req.body)

  menus

    .post(req.body)

    .subscribe((postMenusResponse) => {

      res.send({
        postMenusResponse
      })

    })

}); //POST 

router.put('/', (req, res) => {

  console.log('req.body @ menus put route', req.body)

  menus

    .update(req.body)

    .subscribe((putMenusResponse) => {

      res.send({
        putMenusResponse
      })

    })

}) //PUT

router.get('/', (req, res) => {

  return menus

    .get(req.query)

    .subscribe((getMenusResponse) => {

      res.send(getMenusResponse)

    })

})

router.delete('/', (req, res) => {

  return menus

    .delete({
      _id: req.body._id
    })

    .subscribe((getMenusResponse) => {




      res.send(getMenusResponse)

    })

})


module.exports = router;