const express = require('express');

const router = express.Router();

const request = require('request');

var routes = [
  {
    name: 'health'
  },
  {
    name: 'images'
  },
  {
    name: 'users'
  },
  {
    name: 'menus'
  },
  {
    name: 'menuItems',
    path: "menu-items"
  },

  {
    name: 'orders'
  },
]

routes.forEach((routeConfig) => {

  var routePath = `/${routeConfig.name}`;

  if (routeConfig.path) {
    routePath = `/${routeConfig.path}`;
  }

  var routeSrcPath = `./${routeConfig.name}Route`;

  router.use(routePath, require(routeSrcPath))

})

// GET home page
router.get('/', (req, res) => {

  res.set('Content-Type', 'text/html');

  res.send(fs.readFileSync('./View/build/index.html'));

});

router.post('/', (req, res) => {

  console.log(req.body)
  res.send(req.body);

});

router.post('/login', (req, res) => {

  request.post({
    url: 'https://app.workchew.com/orderslogin',

    body: JSON.stringify({
      token: req.body.token,
      bid: req.body.bid

    }),

    headers: {

      "Content-Type": "application/json"

    }

  }, function(err, httpResponse, body) {

    console.log('venue post', body)

    res.send(JSON.parse(body));

  })

});

module.exports = router;