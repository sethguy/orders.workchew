const Rx = require('rxjs')

const express = require('express');

const router = express.Router();

const menuItems = require('../NodeServices/menuItemsService.js')

const menusService = require('../NodeServices/menusService.js')

const request = require('request');

var verifyUser = function(token, bid) {

  return Rx.Observable.create(function(observer) {

    request.post({
      url: 'https://app.workchew.com/orderslogin',

      body: JSON.stringify({
        token: token,
        bid: bid

      }),

      headers: {

        "Content-Type": "application/json"

      }

    }, function(err, httpResponse, body) {

      observer.next(JSON.parse(body))

      observer.complete()

    })

  })

} //verifyUser

var itemCheck = function(_id, mid) {

  if (_id && _id.length) {

    return getItemAndMenu(_id)

  }

  if (!_id && mid && mid.length) {

    return getMenu(mid)

  }

}

var checkAccessToken = function(req, res, next) {

  var token = req.headers['x-api-access-token'];

  var {_id, mid} = req.body;

  itemCheck(_id, mid)

    .subscribe((menu) => {

      var {bid} = menu;

      if (bid && bid.length) {

        verifyUser(token, bid)

          .subscribe((ordersLoginVerifyUserResponse) => {

            if (ordersLoginVerifyUserResponse.user.role == "coChewer") {

              res.status(401).send({
                error: 401,
                msg: "not authorized"
              })

            } else {

              next()

            }

          })
      }

    })

} //checkAccessToken

var getItemAndMenu = function(itemId) {

  return menuItems

    .get({
      _id: itemId
    })

    .map((items) => {


      console.log("at items", items)

      return items[0]
    })

    .map(item => item.mid)

    .switchMap((mid) => {

      console.log("at mid")

      return getMenu(mid)

    })

} //getItemAndMenu

var getMenu = function(mid) {

  return menusService

    .get({
      _id: mid
    })

    .map((items) => {
      return items[0]
    })

} //getItemAndMenu

var MenusItemsAuth = function(req, res, next) {

  var body = req.body;
  var query = req.query;
  var time = new Date().toUTCString();
  var headers = req.headers;
  var method = req.method;
  var params = req.params;
  var route = req.route;
  var files = req.files;
  var cookies = req.cookies;
  var signedCookies = req.signedCookies;
  var url = req.url;

  var accessToken = headers['x-api-access-token']
  if (method.toLowerCase() === "post" ||
    method.toLowerCase() === "put" ||
    method.toLowerCase() === "delete") {

    onUpdateMehtod(req, res, next)

  } else {

    next()

  }

} //MenusItemsAuth

router.use(MenusItemsAuth)

var onUpdateMehtod = function(req, res, next) {

  if (req.headers['x-api-access-token'] && req.headers['x-api-access-token'].length) {

    checkAccessToken(req, res, next)

  } else {

    res.status(401).send({
      error: 401,
      msg: "no access token"
    })

  }

} //onUpdateMehtod

router.post('/', (req, res) => {

  console.log('req.body @ menuItems post route', req.body)

  menuItems

    .post(req.body)

    .subscribe((postMenuitemsResponse) => {

      res.send({
        postMenuitemsResponse
      })

    })

}); //POST 

router.put('/', (req, res) => {

  console.log('req.body @ menuItems put route', req.body)

  menuItems

    .update(req.body)

    .subscribe((putMenuitemsResponse) => {

      res.send({
        putMenuitemsResponse
      })

    })

}) //PUT

router.get('/', (req, res) => {

  return menuItems

    .get(req.query)

    .subscribe((getMenuitemsResponse) => {

      res.send(getMenuitemsResponse)

    })

})

router.delete('/', (req, res) => {

  return menuItems

    .delete({
      _id: req.body._id
    })
    .subscribe((getMenuitemsResponse) => {

      res.send(getMenuitemsResponse)

    })

})

module.exports = router;