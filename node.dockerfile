FROM node:latest

MAINTAINER Seth Terry

ENV NODE_ENV=development 

ENV NODE_HTTP_PORT=8080

ENV NODE_HTTPS_PORT=443

ENV SSL_START=ON

ENV MONGO_IP=mongodb

RUN npm install forever -g

COPY package.json /orders-wordchew/package.json

RUN cd orders-wordchew; npm install

#ADD certs /orders-wordchew/certs

ADD server.js /orders-wordchew/server.js

ADD View/build /orders-wordchew/View/build

ADD routes /orders-wordchew/routes

ADD middleWare /orders-wordchew/middleWare

ADD rxFormidable /orders-wordchew/rxFormidable

ADD NodeServices /orders-wordchew/NodeServices

WORKDIR   /orders-wordchew

EXPOSE $NODE_HTTP_PORT

EXPOSE $NODE_HTTPS_PORT

ENTRYPOINT ["forever", "server.js"]